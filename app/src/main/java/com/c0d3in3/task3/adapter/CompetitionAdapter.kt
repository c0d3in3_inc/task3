package com.c0d3in3.task3.adapter

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.task3.R
import com.c0d3in3.task3.model.AreaModel
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.team_item_layout.view.*

class CompetitionAdapter(private val items: ArrayList<AreaModel>, private val context: Context) : RecyclerView.Adapter<CompetitionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.team_item_layout,parent,false))
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val competitionModel = items[adapterPosition] as AreaModel
            itemView.teamIdTextView.text = "Team ID: ${competitionModel.id}"
            itemView.teamNameTextView.text = "Team name: ${competitionModel.name}"
            itemView.teamCodeTextView.text = "Team code: ${competitionModel.countryCode}"
            if(competitionModel.ensignUrl != null) GlideToVectorYou.justLoadImage(context as Activity, Uri.parse(competitionModel.ensignUrl), itemView.teamImageView)
            else itemView.teamImageView.setImageResource(R.mipmap.ic_launcher)
        }
    }

}
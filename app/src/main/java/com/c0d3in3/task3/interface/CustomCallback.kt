package com.c0d3in3.task3.`interface`

interface CustomCallback {
    fun onSuccess(response: String)
    fun onFail(throwable: Throwable)
}
package com.c0d3in3.task3.model

import com.c0d3in3.task3.`interface`.DataInterface

class Competition {
    var count: Int? = 0
    var filters : String? = ""
    var competitions = arrayListOf<CompetitionModel>()

}

class CompetitionModel : DataInterface {
    var id : Int? = 0
    var area = arrayListOf<AreaModel>()
    var name : String? = ""
    var code : String? = null
    var emblemUrl : String? = null
    var plan : String? = ""
    var currentSeason = arrayListOf<CurrentSeasonModel>()
    var numberOfAvailableSeasons : Int? = 0
    var lastUpdated : String? = ""
}

class AreaModel : DataInterface{
    var id : Int? = 0
    var name : String? = ""
    var countryCode : String? = ""
    var ensignUrl : String? = null
}

class CurrentSeasonModel{
    var id : Int? = 0
    var startDate : String? = ""
    var endDate : String? = ""
    var currentMatchDay : Int? = 0
    var winner : Boolean? = null
}

class WinnerModel{
    var id : Int? = 0
    var name : String? = ""
    var shortName : String? = ""
    var tla : String? = ""
    var crestUrl : String? = null
}
package com.c0d3in3.task3.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.task3.R
import com.c0d3in3.task3.`interface`.CustomCallback
import com.c0d3in3.task3.adapter.CompetitionAdapter
import com.c0d3in3.task3.model.AreaModel
import com.c0d3in3.task3.model.Competition
import com.c0d3in3.task3.model.CompetitionModel
import com.c0d3in3.task3.network.NetworkHandler
import com.c0d3in3.task3.network.NetworkHandler.COMPETITIONS
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val competition = Competition()
    private val competitionsList = arrayListOf<AreaModel>()
    private var adapter = CompetitionAdapter(competitionsList, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){

        dataLoadingTextView.visibility = View.VISIBLE
        teamRecyclerView.layoutManager = LinearLayoutManager(this)
        teamRecyclerView.adapter = adapter
        makeRequest(COMPETITIONS)

        swipeLayout.setOnRefreshListener {
            refresh()
        }
    }

    private fun refresh(){
        swipeLayout.isRefreshing = true
        competitionsList.clear()
        adapter.notifyDataSetChanged()
        dataLoadingTextView.visibility = View.VISIBLE
        makeRequest(COMPETITIONS)
    }

    private fun makeRequest(path : String){
        NetworkHandler.onGetRequest(path, object: CustomCallback{
            override fun onSuccess(response: String) {
                parseJson(response)
                println(response)
            }

            override fun onFail(throwable: Throwable) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun parseJson(response : String){
        val json = JSONObject(response)
        if(json.has("count")) competition.count = json.getInt("count")
        if(json.has("filters")) competition.filters = json.getString("filters")
        if(json.has("competitions")){
            val competitions = json.getJSONArray("competitions")
            for(i in 0 until competitions.length()){
                val mObject = competitions.getJSONObject(i)
                //val competitionModel = CompetitionModel()
                //if(mObject.has("id"))  competitionModel.id = mObject.getInt("id")
                if(mObject.has("area")){
                    val mArea = mObject.getJSONObject("area")
                    val areaModel = AreaModel()
                    if(!mArea.isNull("id")) areaModel.id = mArea.getInt("id")
                    if(!mArea.isNull("name")) areaModel.name = mArea.getString("name")
                    if(!mArea.isNull("countryCode")) areaModel.countryCode = mArea.getString("countryCode")
                    if(!mArea.isNull("ensignUrl")) areaModel.ensignUrl = mArea.getString("ensignUrl")
                    //competitionModel.area.add(areaModel)
                    competitionsList.add(areaModel)
                }
//
//                if(mObject.has("name"))  competitionModel.name = mObject.getString("name")
//                if(mObject.has("code"))  competitionModel.code = mObject.getString("code")
//                if(mObject.has("emblemUrl"))  competitionModel.emblemUrl = mObject.getString("emblemUrl")
//                if(mObject.has("plan"))  competitionModel.plan = mObject.getString("plan")
            }
        }

        teamRecyclerView.adapter = adapter
        dataLoadingTextView.visibility = View.GONE
        swipeLayout.isRefreshing = false
        adapter.notifyDataSetChanged()
    }
}
